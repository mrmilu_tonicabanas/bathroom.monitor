from .common import *

DEBUG = False
#ALLOWED_HOSTS = [env("DJANGO_ALLOWED_HOSTS")]
ALLOWED_HOSTS = ['*']
SECRET_KEY = env("DJANGO_SECRET_KEY")

DATABASES = {
    'default': env.db("DJANGO_DATABASE_URL", default="mysql://root:root@localhost/develop"),
}