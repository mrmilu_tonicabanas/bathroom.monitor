from .common import *

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '5l!w&pg=$st4asf1^a-br%)1$+&kw%1n9x94$7ghy#h2px-r&3'