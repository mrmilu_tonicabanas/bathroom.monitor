from django.conf.urls import url
from bathroom.API.views import BathroomAPIView
from views.views import Home

urlpatterns = [
    url(r'^api/bathroom_updates/(?P<room>[0-9]+)', BathroomAPIView.as_view()),
    url(r'^$', Home.as_view(), name="home"),
]
