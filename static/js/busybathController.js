var busybath = angular.module('busybath', []);

busybath.controller('BusybathController', ['$scope', '$http', '$interval', function ($scope, $http, $interval) {
  $scope.isBusy = isBusy;
  $scope.getStatusText = getStatusText;


  function isBusy() {
    if ($scope.occupied)
      return true;
    else
      return false;
  }

  function getStatusText() {
    if (isBusy())
      return "Sorry! It's busy...";
    else
      return "Nobody here... Enjoy it!";

  }

  function getStatus() {
    $http.get('http://bathroom.mrmilu.com/api/bathroom_updates/1').success(function (data) {
      $scope.occupied = data.occupied;
      console.log(data);
    });
  }

  $interval(getStatus, 1000);


}]);