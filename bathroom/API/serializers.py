from rest_framework import serializers

from .models import Bathroom


class BathroomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bathroom
        fields = ('room', 'occupied')
