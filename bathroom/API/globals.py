class BathroomStatus(object):
    UNOCCUPIED = 0
    OCCUPIED = 1

    CHOICES = (
        (UNOCCUPIED, 'unoccupied'),
        (OCCUPIED, 'occupied'),
    )
